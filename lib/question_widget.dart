import 'package:flutter/material.dart';
import 'package:project_game/answer.dart';

class Gamepage extends StatefulWidget {
  @override
  _GamepageState createState() => _GamepageState();
}

class _GamepageState extends State<Gamepage> {
  List<Icon> _scorTracker = [
    Icon(
      Icons.check_circle,
      color: Colors.green.shade400,
    ),
    Icon(
      Icons.clear,
      color: Colors.red.shade400,
    ),
  ];

  int _questionIndex = 0;
  int _totalScore = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.purple.shade100,
      body: Center(
        child: Column(
          children: [
            Row(
              children: [
                if (_scorTracker.length == 0)
                  SizedBox(
                    height: 25.0,
                  ),
                if (_scorTracker.length > 0) ..._scorTracker
              ],
            ),
            Container(
              height: 300.0,
              margin: EdgeInsets.only(bottom: 3.0, left: 30.0, right: 30.0),
              padding: EdgeInsets.symmetric(horizontal: 50.0, vertical: 20.0),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(
                      'https://dkt6rvnu67rqj.cloudfront.net/cdn/ff/zqkf-jf8giOA7J21vXDDaXoOSvtnxjzFvSUHiWHqVkw/1579043087/public/styles/600x400/public/media/1015142-flip.jpg?h=c51ba6fe&itok=jEsF24iU'),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10.0),
              child: Center(
                child: Text(
                  'what animal is this picture',
                  style: TextStyle(fontSize: 20.0, color: Colors.white),
                ),
              ),
            ),
            ...(_questions[_questionIndex]['answers']
                    as List<Map<String, dynamic>>)
                .map((answer) {
              return Answer(answerText: answer['answerText']);
            }),
            SizedBox(
              height: 20.0,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                  minimumSize: Size(double.infinity, 40.0)),
              onPressed: () {},
              child: Text('Next'),
            ),
            Container(
              padding: EdgeInsets.all(10.0),
              child: Text(
                '0/9',
                style: TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.purple.shade600),
              ),
            )
          ],
        ),
      ),
    );
  }
}

final _questions = const [
  {
    'question': 'what animal is this picture',
    'answers': [
      {'answerText': 'Tiger', 'score': true},
      {'answerText': 'Dog', 'score': false},
      {'answerText': 'kangaroo', 'score': false},
      {'answerText': 'Cat', 'score': false},
    ],
  },
  {
    'question': 'what animal is this picture',
    'answers': [
      {'answerText': 'seal', 'score': false},
      {'answerText': 'Lion', 'score': false},
      {'answerText': 'cat', 'score': false},
      {'answerText': 'zebra', 'score': true},
    ],
  },
  {
    'question': 'what animal is this picture',
    'answers': [
      {'answerText': 'monkey', 'score': false},
      {'answerText': 'Lion', 'score': false},
      {'answerText': 'giraffe', 'score': true},
      {'answerText': 'turtle', 'score': false},
    ],
  },
  {
    'question': 'what animal is this picture',
    'answers': [
      {'answerText': 'turtle', 'score': true},
      {'answerText': 'monkey', 'score': false},
      {'answerText': 'dog', 'score': false},
      {'answerText': 'crocodile', 'score': false},
    ],
  },
  {
    'question': 'what animal is this picture',
    'answers': [
      {'answerText': 'crocodile', 'score': false},
      {'answerText': 'hippopotamus', 'score': true},
      {'answerText': 'monkey', 'score': false},
      {'answerText': 'Dolphin', 'score': false},
    ],
  },
];
